# Network Peering
# https://cloud.google.com/vpc/docs/quota#vpc-peering
resource "google_compute_network_peering" "stress-testing-a-to-b" {
  name         = "stress-testing-a-to-b"
  network      = google_compute_network.stress-testing-a.self_link
  peer_network = google_compute_network.stress-testing-b.self_link
}

resource "google_compute_network_peering" "stress-testing-b-to-a" {
  name         = "stress-testing-b-to-a"
  network      = google_compute_network.stress-testing-b.self_link
  peer_network = google_compute_network.stress-testing-a.self_link
}
