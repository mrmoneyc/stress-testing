#!/bin/bash

## ------------------------------------------------------- ##
## REMEMBER TO MODIFY THESE VARIABLES TO THE VALID VALUES! ##
## ------------------------------------------------------- ##
TARGET_HOST="<DOMAIN-WITHOUT-PROTOCOL>"
REDIS_HOST="redis://<CONTROL-SERVER-IP>:6379?password=<PASSWORD>"
NPM_START=1

# Add administrators SSH keys here
su ubuntu -c "touch /home/ubuntu/.ssh/authorized_keys && truncate -s 0 /home/ubuntu/.ssh/authorized_keys && chmod 0600 /home/ubuntu/.ssh/authorized_keys"

# Setup essential environment variables
su ubuntu -c "echo 'export TARGET_HOST=${TARGET_HOST}' >> /home/ubuntu/.profile"
su ubuntu -c "echo 'export REDIS_HOST=${REDIS_HOST}' >> /home/ubuntu/.profile"


# Launch test script
if [ "$NPM_START" = "1" ]
then
  su ubuntu -c ". \"/home/ubuntu/.nvm/nvm.sh\" && cd /home/ubuntu/stress-test-bot && TARGET_HOST=${TARGET_HOST} REDIS_HOST=${REDIS_HOST} npm start"
fi
