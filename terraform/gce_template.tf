# Build stress testing GCE related resources
resource "google_compute_instance_template" "stress-testing-a" {
  depends_on = [
    google_compute_network.stress-testing-a,
    google_compute_subnetwork.stress-testing-a,
  ]
  count        = 8
  name         = "stress-testing-a-${count.index + 1}-${random_id.instance-template.hex}"
  machine_type = "custom-2-4096"
  region       = "asia-east1"

  disk {
    source_image = var.stress_testing_instance_template_image
    auto_delete  = true
    boot         = true
    disk_type    = "pd-standard"
  }

  network_interface {
    subnetwork = element(
      google_compute_subnetwork.stress-testing-a.*.name,
      count.index,
    )

    access_config {
      # this is required for getting public IP for our instances even though its value is empty.
      nat_ip = ""
    }
  }

  tags = [
    "stress-testing-all",
    "stress-testing-client-instance",
    "stress-testing-a-${count.index + 1}",
  ]

  metadata = {
    startup-script = file("${path.module}/startup-scripts/default.sh")
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "google_compute_instance_template" "stress-testing-b" {
  depends_on = [
    google_compute_network.stress-testing-b,
    google_compute_subnetwork.stress-testing-b,
  ]
  count        = 8
  name         = "stress-testing-b-${count.index + 1}-${random_id.instance-template.hex}"
  machine_type = "custom-2-4096"
  region       = "asia-east1"

  disk {
    source_image = var.stress_testing_instance_template_image
    auto_delete  = true
    boot         = true
    disk_type    = "pd-standard"
  }

  network_interface {
    subnetwork = element(
      google_compute_subnetwork.stress-testing-b.*.name,
      count.index,
    )

    access_config {
      # this is required for getting public IP for our instances even though its empty value.
      nat_ip = ""
    }
  }

  tags = [
    "stress-testing-all",
    "stress-testing-client-instance",
    "stress-testing-b-${count.index + 1}",
  ]

  metadata = {
    startup-script = file("${path.module}/startup-scripts/default.sh")
  }

  lifecycle {
    create_before_destroy = true
  }
}
