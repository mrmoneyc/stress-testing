terraform {
  required_version = ">= 0.12.2"
  backend "gcs" {
    bucket = "" # TODO: fill gcs bucket & path for terraform state store
    path   = ""
  }
}

provider "google" {
  project = var.project
  region  = var.region["tw"]
}

resource "random_id" "instance-template" {
  keepers = {
    ami_id         = var.stress_testing_instance_template_image
    startup_script = md5(file("${path.module}/startup-scripts/default.sh"))
  }
  byte_length = 8
}
