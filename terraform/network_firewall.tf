# Create firewall rule to allow global SSH access to 'stress-testing-instance'!
resource "google_compute_firewall" "stress-testing-allow-ssh-a" {
  name    = "stress-testing-network-ssh-a"
  network = google_compute_network.stress-testing-a.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["stress-testing-all"]
}

resource "google_compute_firewall" "stress-testing-allow-ssh-b" {
  name    = "stress-testing-network-ssh-b"
  network = google_compute_network.stress-testing-b.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["stress-testing-all"]
}

# Create firewall rule to allow internal Redis access to 'stress-testing-control-server'!
resource "google_compute_firewall" "stress-testing-allow-internal-redis-access" {
  name    = "stress-testing-network-redis-internal"
  network = google_compute_network.stress-testing-a.name

  allow {
    protocol = "tcp"
    ports    = ["6379"]
  }

  source_ranges = ["10.0.0.0/8"]
  target_tags   = ["stress-testing-control-server"]
}
