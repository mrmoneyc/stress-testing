# Instance groups (via Regional Instance Group Manager API)
resource "google_compute_region_instance_group_manager" "stress-testing-a" {
  depends_on         = [google_compute_instance_template.stress-testing-a]
  count              = 8
  name               = "stress-testing-a-${count.index + 1}"
  region             = "asia-east1"
  base_instance_name = "stress-testing-a-${count.index + 1}"
  version {
    instance_template = element(
      google_compute_instance_template.stress-testing-a.*.self_link,
      count.index,
    )
  }


  # >>> Seeking for the place to adjust the number of instances? Target size is what you are looking for! <<<
  target_size = var.group_a_target_size
}

resource "google_compute_region_instance_group_manager" "stress-testing-b" {
  depends_on         = [google_compute_instance_template.stress-testing-b]
  count              = 8
  name               = "stress-testing-b-${count.index + 1}"
  region             = "asia-east1"
  base_instance_name = "stress-testing-b-${count.index + 1}"
  version {
    instance_template = element(
      google_compute_instance_template.stress-testing-b.*.self_link,
      count.index,
    )
  }

  # >>> Seeking for the place to adjust the number of instances? Target size is what you are looking for! <<<
  target_size = var.group_b_target_size
}
