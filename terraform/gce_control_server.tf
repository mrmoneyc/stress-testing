# Stress testing control server GCE instance
resource "google_compute_instance" "stress-testing-control-server-instance" {
  depends_on = [
    google_compute_network.stress-testing-a,
    google_compute_subnetwork.stress-testing-a,
  ]
  name         = "stress-testing-control-server"
  machine_type = "n1-standard-4"
  zone         = "asia-east1-a"

  network_interface {
    subnetwork = google_compute_subnetwork.stress-testing-a[0].name
    network_ip = "" # TODO: fill control server private ip here
    access_config {
      nat_ip = "" # TODO: fill control server public ip here
    }
  }

  boot_disk {
    initialize_params {
      size  = 20
      type  = "pd-standard"
      image = var.stress_testing_instance_template_image
    }
  }

  metadata_startup_script = file("./startup-scripts/default.sh")

  tags = [
    "stress-testing-control-server",
    "stress-testing-all",
  ]

  lifecycle {
    ignore_changes = [
      # boot_disk,
    ]
  }

}
