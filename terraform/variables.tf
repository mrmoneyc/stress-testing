variable "project" {
  default = "" # TODO: fill gcp project id here
}

variable "region" {
  type = map(string)

  default = {
    tw = "asia-east1"
    jp = "asia-northeast1"
  }
}

variable "zone" {
  default = "asia-east1-a"
}

variable "network" {
  default = "stress-testing-network"
}

variable "stress_testing_instance_template_image" {
  default = "" # TODO: fill base image here (built by packer)
}

variable "machine_type" {
  default = "custom-2-4096"
}

variable "group_a_target_size" {
  default = 0
}

variable "group_b_target_size" {
  default = 0
}
