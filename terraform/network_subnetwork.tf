# Subnets
resource "google_compute_subnetwork" "stress-testing-a" {
  count         = 8
  name          = "stress-testing-a-${count.index + 1}"
  ip_cidr_range = "10.${count.index * 4 + 128}.0.0/20"
  network       = google_compute_network.stress-testing-a.self_link
}

resource "google_compute_subnetwork" "stress-testing-b" {
  count         = 8
  name          = "stress-testing-b-${count.index + 9}"
  ip_cidr_range = "10.${count.index * 4 + 192}.0.0/20"
  network       = google_compute_network.stress-testing-b.self_link
}
