# Network
# 2019 IMPORTANT: We can only launch 15000 machine per VPC
# https://cloud.google.com/vpc/docs/quota#per_network
resource "google_compute_network" "stress-testing-a" {
  project                 = var.project
  name                    = "${var.network}-a"
  auto_create_subnetworks = "false"
}

resource "google_compute_network" "stress-testing-b" {
  project                 = var.project
  name                    = "${var.network}-b"
  auto_create_subnetworks = "false"
}
