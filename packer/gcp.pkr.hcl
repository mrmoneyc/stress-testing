locals {
  timestamp = "${formatdate("YYYYMMDDhhmmss", timestamp())}"
}

variable "account_file" {
  default = "account.json"
}

variable "project_id" {
  default = ""
  validation {
    condition = length(var.project_id) > 0
    error_message = "The project id is not set."
  }
}

source "googlecompute" "stress-test" {
  account_file = var.account_file
  project_id = var.project_id
  source_image = "ubuntu-2004-focal-v20210720"
  zone = "asia-east1-a"
  image_description = "Stress Testing Image (${local.timestamp})"
  image_name = "stress-test-${local.timestamp}"
  network = "stress-testing-network-a"
  subnetwork = "stress-testing-a-1"
  ssh_username = "ubuntu"
  tags = [
    "stress-testing-instance",
    "stress-testing-all",
    "stress-testing-control-server"
  ]
}

build {
  sources = ["source.googlecompute.stress-test"]

  provisioner "shell-local" {
    inline = [
      "rm -rf ../stress-test-bot/node_modules" // specific stress-test-bot path
    ]
  }

  provisioner "file" {
    source = "../stress-test-bot" // specific stress-test-bot path
    destination = "/home/ubuntu"
  }

  provisioner "shell" {
    script = "provision-gcp.sh"
  }
}
