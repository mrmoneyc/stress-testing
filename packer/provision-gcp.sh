#!/usr/bin/env bash

echo "Configure timezone & locale"
sudo timedatectl set-timezone Asia/Taipei
sudo locale-gen zh_TW.UTF-8
echo 'export LC_ALL=en_US.UTF-8' >> "$HOME/.profile"

echo "Install required packages"
sudo apt-get -y update && sudo apt-get -y upgrade
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install zip unzip redis-server fontconfig build-essential

echo "Install chrome dependencies"
sudo apt-get install -y wget unzip fontconfig locales gconf-service \
libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 \
libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 \
libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 \
libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates \
fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget libgbm-dev

echo "Install nvm (Node Version Manager)"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
export NVM_DIR="/home/ubuntu/.nvm"
[ -s "$NVM_DIR/nvm.sh"  ] && . "$NVM_DIR/nvm.sh"

echo "Install Node via NVM"
nvm install v14.17.5

echo "Make Redis server listens at *:6379"
sudo sed -i '/^bind\s127\.0\.0\.1/d' /etc/redis/redis.conf
sudo sed -i 's/^\#\srequirepass\sfoobared/requirepass PASSWORD/g' /etc/redis/redis.conf # TODO: update redis password

echo "Run 'npm install'"
cd "$HOME/stress-test-bot"
npm install

echo "Append NPM binary dir. to \$PATH"
echo 'export PATH=$PATH:$(cd $HOME/stress-test-bot/; npm bin)' >> "$HOME/.profile"

echo "Increase limit of open files to 200000"
sudo sh -c "echo '* soft nofile 200000' >> /etc/security/limits.conf"
sudo sh -c "echo '* hard nofile 200000' >> /etc/security/limits.conf"

echo "Clean APT caches"
sudo apt-get -y clean

echo "Done!"
